//
//  DetailsViewController.swift
//  tableView
//
//  Created by Pardip Bhatti on 09/04/17.
//  Copyright © 2017 gpCoders. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {
    
    @IBOutlet weak var Emoji: UILabel!
    @IBOutlet weak var DetailsName: UILabel!
    var ActName = "Empty"
    override func viewDidLoad() {
        super.viewDidLoad()
        if ActName == "🤡" {
            Emoji.text = self.ActName
            DetailsName.text = self.ActName
        } else {
            DetailsName.text = self.ActName
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}

//
//  ViewController.swift
//  tableView
//
//  Created by Pardip Bhatti on 09/04/17.
//  Copyright © 2017 gpCoders. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var gpTableView: UITableView!
    
    // List Array
    var gpNames: [String] = ["Gugu", "Bubu", "Gagan", "Pardip", "Sam", "Amka", "Shalu", "🤡"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        gpTableView.dataSource = self
        gpTableView.delegate = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return gpNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        
        cell.textLabel?.text = gpNames[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // Deselct row after back
        tableView.deselectRow(at: indexPath, animated: true)
        
        let gpName = gpNames[indexPath.row]
        
        performSegue(withIdentifier: "tableDetails", sender: gpName)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let detailsVC = segue.destination as! DetailsViewController
        detailsVC.ActName = sender as! String
    }
    

}

